# Microsoft SQL Server ODBC Driver 18

## Installation
> ⚠ The setup script will prompt for elevated permssions to ensure that this driver is registered with your ODBC configuration at `/etc/odbcinst.ini`.

1. `devbox install`
2. `devbox run setup`
